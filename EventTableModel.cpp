#include "EventTableModel.h"
#include <QDebug>
#include <QSqlError>
#include <QColor>

/*
select BYDY_NO,A.TARGET_NO,SYS_TIME,W_AMP,W_TIME,AMP,TIME,WATER_TEMP from (select * from (select * FROM
'raw_2020-000001' ORDER BY SYS_TIME DESC) GROUP by TARGET_NO) AS A INNER JOIN 'setup_2020-000001' on
A.TARGET_NO = 'setup_2020-000001'.TARGET_NO
*/
EventTableModel::EventTableModel(QSqlDatabase db)
{
    m_tableModel=new QSqlQueryModel;
    m_db = db;
    //m_db.open();
}
QVariant EventTableModel::data(const QModelIndex &idx, int role) const
{
    if(role == Qt::TextAlignmentRole)
        return Qt::AlignCenter;
    else if(role == Qt::BackgroundColorRole)
    {
        //return QVariant(QColor(Qt::black));
    }
    else if(role == Qt::DisplayRole && (idx.column() == 3 || idx.column() == 5))
    {
        return  QString::number(QIdentityProxyModel::data(idx, role).toDouble(),'f',2);
    }

    return QIdentityProxyModel::data(idx, role);
}
bool EventTableModel::setTable(QString tableName)
{

    QString raw = QString("raw_%1").arg(tableName);
    QString setup = QString("setup_%1").arg(tableName);
    QString query = QString("select BYDY_NO,A.TARGET_NO,SYS_TIME,W_AMP,W_TIME,AMP,TIME,WATER_TEMP from (select * from (select * FROM \
                            '%1' ORDER BY SYS_TIME DESC) GROUP by TARGET_NO) AS A INNER JOIN '%2' on A.TARGET_NO = '%2'.TARGET_NO").arg(raw).arg(setup);

    m_tableModel->setQuery(query,m_db);

    if(m_tableModel->lastError().isValid())
    {
        qDebug() << m_tableModel->lastError();
        return false;
    }
    this->setSourceModel(m_tableModel);
    return true;
}

/*
select BYDY_NO,A.TARGET_NO,SYS_TIME,W_AMP,W_TIME,AMP,TIME,WATER_TEMP,
    case
        when (ABS(W_AMP - AMP) > 1) then
            '불량'
        when (ABS(W_AMP - AMP) > 0.6) then
            '경고H'
        when (ABS(W_AMP - AMP) > 0.2) then
            '경고L'
        ELSE
            '정상'
        end
    from (select * from (select * FROM 'raw_2020-000002' ORDER BY SYS_TIME DESC) GROUP by TARGET_NO) AS A
    INNER JOIN 'setup_2020-000002' on A.TARGET_NO = 'setup_2020-000002'.TARGET_NO
*/
