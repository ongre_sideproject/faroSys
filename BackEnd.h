#ifndef CSERIALPORTSEARCHCLASS_H
#define CSERIALPORTSEARCHCLASS_H

#include <QObject>
#include <QSerialPortInfo>
#include <QSerialPort>
#include <QTimer>
#include "DataTableModel.h"
#include <QMutex>
#include "Defines.h"
#include <QDateTime>
class BackEnd : public QObject
{
    Q_OBJECT
public:
    explicit BackEnd(QObject *parent = nullptr);
    ~BackEnd();

public slots:
    void initThread();
    void onConnectSerialPortByName(const QString portName, const int baudrate, const int interval, QSqlDatabase db);
    void onSaveFunction(const DataTableModel* dataTableModel, const QString filename);
    void onStartFunction(bool tf);
    void onAddData(const DataStruct data, bool isNew);
    void safeQuit();

signals:
    void serialPortListChanged(const QList<QSerialPortInfo> list);
    void appendData(const QString data);
    void serialStatusChanged(const bool status);
    void sendData(const QString data);
    void deviceStatusChanged(const bool tf, QString msg = "");

private slots:
    void onSerialInfoTimeout(void);
    void onCanReadSerialPort(void);
    void onSerialPortStatusTimeout(void);
    void onFuntionTimeout(void);

private:
    bool isSerialOpen(void);
    void exportByCSV(const DataTableModel* dataTableModel, QString filename);
    bool setRTC();
    void createNewTable(QString tableName, QSqlDatabase db);
    void appendRawData(const DataStruct data, QSqlDatabase db);


    QTimer *                m_serialInfoTimer;
    QTimer *                m_serialPortStatusTimer;
    QTimer *                m_functionTimer;
    QTimer *                m_commitTimer;

    QList<QSerialPortInfo>  m_serialPortList;
    QSerialPort *           m_serialPort;
    QString                 m_buffer;
    bool                    m_status;
    QMutex                  m_mutex;
    int                     m_currentIndex;
    int                     m_prevIndex;
    QDateTime               m_prevTime;
    bool                    m_isQuit;
    bool                    m_dbActive;
    bool                    m_isDeviceStatus;
    QSqlDatabase            m_db;
};

#endif // CSERIALPORTSEARCHCLASS_H
