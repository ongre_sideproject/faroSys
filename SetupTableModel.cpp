#include "SetupTableModel.h"
#include <QDebug>

SetupTableModel::SetupTableModel(QSqlDatabase db)
{
    m_tableModel =  new QSqlTableModel(this,db);
}

QVariant SetupTableModel::data(const QModelIndex &idx, int role) const
{
    if(role == Qt::TextAlignmentRole)
        return Qt::AlignCenter;
    else if(role == Qt::DisplayRole && idx.column() == 3)
    {
        return  QString::number(QIdentityProxyModel::data(idx, role).toDouble(),'f',2);
    }else if(role == Qt::EditRole && (idx.column() == 1 || idx.column() == 3 || idx.column() == 4))
    {
        return QIdentityProxyModel::data(idx, role).toDouble();
    }
    return QIdentityProxyModel::data(idx, role);
}
bool SetupTableModel::setTable(QString tableName)
{
    m_tableModel->setTable(tableName);
    m_tableModel->setEditStrategy(QSqlTableModel::OnManualSubmit);
    m_tableModel->select();
    this->setSourceModel(m_tableModel);
    return true;
}
bool SetupTableModel::onSave()
{
    return m_tableModel->submitAll();
}
bool SetupTableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(index.column() == 3 && (value.toDouble() < 1.0 || value.toDouble() > 10.99))
    {

        return true;
    }

    return QIdentityProxyModel::setData (index, value, role);
}
