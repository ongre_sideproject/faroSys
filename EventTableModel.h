#ifndef EVENTTABLEMODEL_H
#define EVENTTABLEMODEL_H

#include <QObject>
#include <QSqlQueryModel>
#include <QIdentityProxyModel>

class EventTableModel : public QIdentityProxyModel
{
    Q_OBJECT
public:
    EventTableModel(QSqlDatabase db = QSqlDatabase());


    QVariant data(const QModelIndex &idx, int role = Qt::DisplayRole) const override;
    bool setTable(QString tableName);

private:
    QSqlQueryModel *    m_tableModel;
    QSqlDatabase        m_db;
};

#endif // EVENTTABLEMODEL_H
