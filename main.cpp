#include "mainwindow.h"

#include <QApplication>
#include <QThread>
#include "BackEnd.h"
#include "DataTableModel.h"
#include "Defines.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    BackEnd backend;
    MainWindow w;
    qRegisterMetaType<QList<QSerialPortInfo>>("QList<QSerialPortInfo>");
    qRegisterMetaType<DataStruct>("DataStruct");
    qRegisterMetaType<QSqlDatabase>("QSqlDatabase");

    QObject::connect(&backend, &BackEnd::serialPortListChanged, &w, &MainWindow::onSerialPortListChanged,Qt::QueuedConnection);
    QObject::connect(&backend, &BackEnd::appendData, &w, &MainWindow::onAppendData,Qt::QueuedConnection);
    QObject::connect(&backend, &BackEnd::serialStatusChanged, &w, &MainWindow::onSerialPortStatusChanged,Qt::QueuedConnection);

    QObject::connect(&backend, &BackEnd::sendData, &w, &MainWindow::onSendData,Qt::QueuedConnection);
    QObject::connect(&backend, &BackEnd::deviceStatusChanged, &w, &MainWindow::onDeviceStatusChanged,Qt::QueuedConnection);

    QObject::connect(&w, &MainWindow::connectSerialPortByName, &backend, &BackEnd::onConnectSerialPortByName,Qt::QueuedConnection);
    QObject::connect(&w, &MainWindow::saveFunction, &backend, &BackEnd::onSaveFunction,Qt::QueuedConnection);
    QObject::connect(&w, &MainWindow::startFunction, &backend, &BackEnd::onStartFunction,Qt::QueuedConnection);
    QObject::connect(&w, &MainWindow::addData, &backend, &BackEnd::onAddData,Qt::QueuedConnection);
    QObject::connect(&w, &MainWindow::safeQuit, &backend, &BackEnd::safeQuit,Qt::DirectConnection);


    QThread * thread = new QThread;
    backend.moveToThread(thread);
    QObject::connect(thread,SIGNAL(started()),&backend,SLOT(initThread()));

    thread->start();
    //w.showFullScreen();
    w.show();
    return a.exec();
}
