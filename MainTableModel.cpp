#include "MainTableModel.h"
#include <QSqlRecord>
#include <QSqlError>
#include <QDebug>

enum MAIN_labels
{
    L_FACTORY,
    L_LINE,
    L_FACT_NO,
    L_ROBOT_NO,
    L_DEVICE_ID,
    L_EVENT
};

MainTableModel::MainTableModel(QSqlDatabase db)
{
    m_tableModel =  new QSqlTableModel(this,db);
}

QVariant MainTableModel::data(const QModelIndex &idx, int role) const
{
    QString value = QIdentityProxyModel::data(idx, role).toString();
    if(role == Qt::TextAlignmentRole)
        return Qt::AlignCenter;
    if(role == Qt::DisplayRole && idx.column() < L_DEVICE_ID && value.isEmpty())
        return QString("미등록");

    return QIdentityProxyModel::data(idx, role);
}
bool MainTableModel::setTable(QString tableName)
{
    m_tableModel->setTable(tableName);
    m_tableModel->setEditStrategy(QSqlTableModel::OnManualSubmit);
    m_tableModel->select();

    m_tableModel->setHeaderData(0, Qt::Horizontal, tr("공장명"));
    m_tableModel->setHeaderData(1, Qt::Horizontal, tr("라인명"));
    m_tableModel->setHeaderData(2, Qt::Horizontal, tr("공장번호"));
    m_tableModel->setHeaderData(3, Qt::Horizontal, tr("로봇번호"));
    m_tableModel->setHeaderData(4, Qt::Horizontal, tr("기기고유번호"));
    m_tableModel->setHeaderData(5, Qt::Horizontal, tr("이벤트내용"));

    this->setSourceModel(m_tableModel);
    return true;
}
bool MainTableModel::onSave()
{
    if(m_tableModel->submitAll())
    {
        return true;
    }
    qDebug() << __FUNCTION__ << m_tableModel->lastError();
    return false;
}
bool MainTableModel::append(QString device_ID)
{
    QSqlRecord record = m_tableModel->record();

    record.setValue(0,"");
    record.setValue(1,"");
    record.setValue(2,"");
    record.setValue(3,"");
    record.setValue(4,device_ID);
    record.setValue(5,"");


    m_tableModel->insertRecord(m_tableModel->rowCount(),record);
    return true;
}
int MainTableModel::isContins(QString device_ID) const
{
    for(int i = 0 ; i < m_tableModel->rowCount() ; i++)
    {
        if(m_tableModel->data(m_tableModel->index(i,L_DEVICE_ID)).toString().compare(device_ID) == 0)
        {
            return i;
        }
    }
    return (-1);
}

QString MainTableModel::getDeviceID(int rowIndex) const
{
    return m_tableModel->data(m_tableModel->index(rowIndex,L_DEVICE_ID)).toString();
}
QStringList MainTableModel::getStringData(int rowIndex) const
{
    QStringList list;
    for(int i = 0 ; i < 4 ; i++)
    {
        list.append(m_tableModel->data(m_tableModel->index(rowIndex,i)).toString());
    }
    return list;

}
bool MainTableModel::setStringData(int rowIndex, QStringList list)
{
    if(list.count() != 4)
        return false;

    m_tableModel->setData(m_tableModel->index(rowIndex,L_FACTORY),list.at(0));
    m_tableModel->setData(m_tableModel->index(rowIndex,L_LINE),list.at(1));
    m_tableModel->setData(m_tableModel->index(rowIndex,L_FACT_NO),list.at(2));
    m_tableModel->setData(m_tableModel->index(rowIndex,L_ROBOT_NO),list.at(3));
    return true;
}
