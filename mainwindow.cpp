#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QStandardPaths>
#include <QDebug>
#include <QDateTime>
#include <QMessageBox>
#include <QSqlDatabase>
#include <QLocale>
#include <QSqlError>
#include <QTimer>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
static const QString greenSS = QString("background-color: rgba(0, 255, 0, 255);");
static const QString blueSS = QString("background-color: rgba(255, 0, 0, 255);");

static const QString redSS = QString("background-color: rgba(255, 0, 0, 255);");
static const QString SerialGraySS = QString("background-color: rgba(107, 123, 148, 255);");
static const QString TxSS = QString("background-color: rgba(31, 255, 186, 255);");
static const QString RxSS = QString("background-color: rgba(243, 255, 191, 255);");
static const QString whiteSS = QString("background-color: rgba(255, 255, 255, 255);");

enum DATA_labels
{
    L_BYDY,
    L_No,
    L_Time,
    L_W_Amp,
    L_W_Time,
    L_R_Amp,
    L_R_Time,
    L_Water,
    L_Grade,
    L_Color
};

enum MAIN_labels
{
    L_FACTORY,
    L_LINE,
    L_FACT_NO,
    L_ROBOT_NO,
    L_DEVICE_ID,
    L_EVENT
};

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    initDB();
    initMainTable();

    onSerialPortStatusChanged(false);
    ui->tableView->verticalHeader()->hide();
    ui->tableView->horizontalHeader()->hide();

    connect(ui->tableView_2, SIGNAL(doubleClicked(const QModelIndex &)), this, SLOT(onClickedMainTableCell(const QModelIndex &)));
    ui->tableView_2->verticalHeader()->hide();
    onResizeColumn();
    initSubUI(MAIN_VIEW);
    m_deviceStatusTimer = new QTimer;
    m_deviceStatusTimer->setInterval(100);
    connect(m_deviceStatusTimer, SIGNAL(timeout()), this, SLOT(deviceStatusTimeout()));

    qDebug() << ui->logo->height();
#if 1
    QImage *image = new QImage();
    if(image->load("logo.jpg"))
    {
        QPixmap *buffer = new QPixmap();
        *buffer = QPixmap::fromImage(*image);
        ui->logo->setPixmap(*buffer);
    }
#endif

#if 1
    if(QDate::currentDate().addDays(3).daysTo(QLocale("en_US").toDate(QString(__DATE__).simplified(), "MMM d yyyy")) >= 0)
    {
        this->deleteLater();
    }
#endif

}

MainWindow::~MainWindow()
{
    emit safeQuit();
    m_MainModel->onSave();
    if(!m_db.commit())
    {
        qDebug() << __FUNCTION__ << m_db.lastError().text();
        qDebug() << __FUNCTION__ << m_db.rollback();
    }
    m_db.close();
    delete ui;
}

void MainWindow::onSerialPortListChanged(const QList<QSerialPortInfo> list)
{
    ui->cbSerialPorts->clear();
    Q_FOREACH (const QSerialPortInfo& port, list) {
        ui->cbSerialPorts->addItem(port.portName());
    }
    loadJson();
}

void MainWindow::on_btnConnect_clicked()
{
    QString tmp = ui->cbSerialPorts->currentText();
    int baudrate, interval;

    switch (ui->cbBaudrate->currentIndex()) {
    case 0: baudrate = 9600; break;
    case 1: baudrate = 19200; break;
    case 2: baudrate = 38400; break;
    default: baudrate = 9600; break;
    }
    switch (ui->cbPollingInterval->currentIndex()) {
    case 0: interval = 300; break;
    case 1: interval = 500; break;
    case 2: interval = 900; break;
    default: interval = 300; break;
    }

    emit connectSerialPortByName(tmp,baudrate,interval,m_db);
    saveJson();
}

void MainWindow::onAppendData(const QString rawData)
{
    QStringList list = rawData.split(",");

    if(list.count() != 7)
    {
        qDebug() << __FUNCTION__ << "size fail" << rawData;
        return;
    }
    list.removeAll("+");
    qDebug() << list;
    ui->serialPortStatus->setStyleSheet(RxSS);
    ui->serialPortStatus->setText(QString("~Rk ").append(rawData));

    QString tmp = list.at(1).left(5);
    tmp.append(list.at(0));

    //qDebug() << __FUNCTION__ << list;

    int isContain = m_MainModel->isContins(tmp);
    if(isContain == (-1))
    {
        m_MainModel->append(tmp);
        emit addData(DataStruct{tmp,list.at(1),list.at(2),list.at(3),list.at(4),list.at(5)},true);
    }
    else
    {
       emit addData(DataStruct{tmp,list.at(1),list.at(2),list.at(3),list.at(4),list.at(5)},false);
    }
}

void MainWindow::onSerialPortStatusChanged(const bool status)
{
    if(status)
    {
        ui->btnConnect->setText("Disconnect");
        ui->serialPortStatus->setStyleSheet(greenSS);
        ui->cbBaudrate->setEnabled(false);
        ui->cbSerialPorts->setEnabled(false);
        ui->cbPollingInterval->setEnabled(false);
    }else
    {
        ui->btnConnect->setText("Connect");
        ui->serialPortStatus->setStyleSheet(SerialGraySS);
        ui->cbBaudrate->setEnabled(true);
        ui->cbSerialPorts->setEnabled(true);
        ui->cbPollingInterval->setEnabled(true);
    }
}




void MainWindow::on_btnTest_2_clicked()
{

}

void MainWindow::on_btnClose_clicked()
{
    QApplication::quit();
}

void MainWindow::on_btnMainTest_clicked()
{
}
void MainWindow::on_btnMainTest_1_clicked()
{
}
void MainWindow::onResizeColumn()
{
    ui->BYDY_NO->setMaximumWidth(ui->tableView->columnWidth(0));
    ui->BYDY_NO->setMinimumWidth(ui->tableView->columnWidth(0));

    ui->NO->setMaximumWidth(ui->tableView-> columnWidth(1));
    ui->NO->setMinimumWidth(ui->tableView->columnWidth(1));

    ui->DateTime->setMaximumWidth(ui->tableView->columnWidth(2));
    ui->DateTime->setMinimumWidth(ui->tableView->columnWidth(2));

    ui->W_AMP->setMaximumWidth(ui->tableView->columnWidth(3));
    ui->W_AMP->setMinimumWidth(ui->tableView->columnWidth(3));

    ui->W_TIME->setMaximumWidth(ui->tableView->columnWidth(4));
    ui->W_TIME->setMinimumWidth(ui->tableView->columnWidth(4));

    ui->R_AMP->setMaximumWidth(ui->tableView->columnWidth(5));
    ui->R_AMP->setMinimumWidth(ui->tableView->columnWidth(5));

    ui->R_TIME->setMaximumWidth(ui->tableView->columnWidth(6));
    ui->R_TIME->setMinimumWidth(ui->tableView->columnWidth(6));

    ui->WATER->setMaximumWidth(ui->tableView->columnWidth(7));
    ui->WATER->setMinimumWidth(ui->tableView->columnWidth(7));

    ui->GRADE->setMaximumWidth(ui->tableView->columnWidth(8));
    ui->GRADE->setMinimumWidth(ui->tableView->columnWidth(8));

    /*
    ui->GCOLOR->setMinimumWidth(ui->tableView->columnWidth(9)+2);
    ui->GCOLOR->setMinimumWidth(ui->tableView->columnWidth(9)+2);
    */
}

void MainWindow::on_btnSearch_clicked()
{
    QString text = ui->edSearch->text();

    int isContain = m_MainModel->isContins(text);
    if(isContain != (-1))
    {
        setTitle(isContain);
        initDataTable(text);
    }else {
        QMessageBox msgBox;
        msgBox.setText("검색 실패");
        msgBox.exec();
    }
}

void MainWindow::initDB()
{
    m_db = QSqlDatabase::addDatabase("QSQLITE","faro");//데이터베이스 종류설정
    m_db.setDatabaseName("faro.db"); //데이터베이스 파일이름설정
    m_db.open();
    qDebug() << __FUNCTION__ <<  m_db.transaction();

    m_SetupModel = new SetupTableModel(m_db);
    m_DataModel = new DataTableModel(m_db);
    m_EventModel = new EventTableModel(m_db);
    m_MainModel = new MainTableModel(m_db);
}
void MainWindow::initMainTable()
{
    m_MainModel->setTable("main");
    ui->tableView_2->setModel(m_MainModel);
    ui->tableView_2->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableView_2->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->tableView_2->horizontalHeader()->setStyleSheet("QHeaderView::section{border: 1px solid #000000;}");
    ui->tableView_2->show();
}



void MainWindow::initSetupTable(QString tableName)
{
    initSubUI(SETUP_VIEW);
    m_SetupModel->setTable(tableName.insert(0,"setup_"));
    m_SetupModel->insertColumn(2);
    m_SetupModel->insertColumns(5,5);

    ui->tableView->setModel(m_SetupModel);
    ui->tableView->horizontalHeader()->setStretchLastSection(true);

    ui->tableView->resizeColumnToContents(1);
    ui->tableView->resizeColumnToContents(3);
    ui->tableView->resizeColumnToContents(4);

    ui->tableView->setColumnWidth(5,ui->tableView->columnWidth(3));
    ui->tableView->setColumnWidth(6,ui->tableView->columnWidth(3));
    ui->tableView->setColumnWidth(7,ui->tableView->columnWidth(3));

    ui->tableView->show();

    //ui->tableView->setColumnWidth()
    //ui->tableView->resizeColumnsToContents();
    onResizeColumn();

}
void MainWindow::initDataTable(QString tableName)
{
    initSubUI(DATA_VIEW);
    m_DataModel->setTable(tableName);
    ui->tableView->setModel(m_DataModel);
    ui->tableView->show();

    ui->tableView->resizeColumnsToContents();
    onResizeColumn();

}
void MainWindow::initEventTable(QString tableName)
{
    initSubUI(EVENT_VIEW);
    m_EventModel->setTable(tableName);
    ui->tableView->setModel(m_EventModel);
    ui->tableView->show();

    ui->tableView->resizeColumnsToContents();
    onResizeColumn();


}

void MainWindow::on_btnCancel_clicked()
{
    initSetupTable(m_MainModel->getDeviceID(m_currentRow));
}

void MainWindow::onClickedMainTableCell(const QModelIndex & index)
{
    if(index.column() > L_DEVICE_ID)
    {
        return;
    }
    setTitle(index.row());
    if(index.column() == L_DEVICE_ID)
    {
        initEventTable(m_MainModel->getDeviceID(index.row()));
    }else {
        initSetupTable(m_MainModel->getDeviceID(index.row()));
    }

}
void MainWindow::initSubUI(VIEW_ENUM status)
{
    switch (status) {
    case MAIN_VIEW:
        emit startFunction(true);
        ui->stackedWidget->setCurrentIndex(0);
        break;
    case SETUP_VIEW:
        emit startFunction(false);
        ui->btnBackButton->setText("저장후 메뉴복귀");
        ui->btnCancel->setText("취소-표시복원");
        ui->btnCancel->show();
        ui->edExportPath->setText("");
        ui->btnExportExcel->hide();
        ui->stackedWidget->setCurrentIndex(1);
        ui->edFactory_Name->setReadOnly(false);
        ui->edLine_Name->setReadOnly(false);
        ui->edFactory_No->setReadOnly(false);
        ui->edRobot_No->setReadOnly(false);
        break;
    case EVENT_VIEW:
        emit startFunction(false);
        ui->btnBackButton->setText("메뉴종료");
        ui->btnCancel->hide();
        ui->edExportPath->setText("");
        ui->btnExportExcel->hide();
        ui->stackedWidget->setCurrentIndex(1);
        ui->edFactory_Name->setReadOnly(true);
        ui->edLine_Name->setReadOnly(true);
        ui->edFactory_No->setReadOnly(true);
        ui->edRobot_No->setReadOnly(true);
        break;
    case DATA_VIEW:
        emit startFunction(false);
        ui->edExportPath->setText(QCoreApplication::applicationDirPath().replace("/","\\"));
        ui->btnCancel->hide();
        ui->btnExportExcel->show();
        ui->stackedWidget->setCurrentIndex(1);
        ui->edFactory_Name->setReadOnly(true);
        ui->edLine_Name->setReadOnly(true);
        ui->edFactory_No->setReadOnly(true);
        ui->edRobot_No->setReadOnly(true);
        break;
    default:
        break;
    }
}

void MainWindow::on_btnBackButton_clicked()
{

    if(!ui->btnBackButton->text().contains("저장후 메뉴복귀"))
    {
        initSubUI(MAIN_VIEW);
        return;
    }

    QStringList list;
    list.append(ui->edFactory_Name->text());
    list.append(ui->edLine_Name->text());
    list.append(ui->edFactory_No->text());
    list.append(ui->edRobot_No->text());
    m_MainModel->setStringData(m_currentRow,list);
    m_SetupModel->onSave();
    initSubUI(MAIN_VIEW);
}

void MainWindow::on_btnExportExcel_clicked()
{
    QString filename = QCoreApplication::applicationDirPath().append("/");
    filename.append(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss"));
    filename.append(".csv");
    emit saveFunction(m_DataModel, filename);
    ui->edExportPath->setText(filename.replace("/","\\"));

}
void MainWindow::setTitle(int index)
{
    QList<QLineEdit*> list;
    list.append(ui->edFactory_Name);
    list.append(ui->edLine_Name);
    list.append(ui->edFactory_No);
    list.append(ui->edRobot_No);


    QStringList getList = m_MainModel->getStringData(index);
    for(int i = 0 ; i < list.count() ; i++)
    {
        list[i]->setText(getList.at(i));
    }

    ui->edDevice_No->setText(m_MainModel->getDeviceID(index));
    m_currentRow = index;

}

void MainWindow::onSendData(const QString data)
{
    ui->serialPortStatus->setText(data);
    if(!m_deviceStatusTimer->isActive())
        ui->serialPortStatus->setStyleSheet(TxSS);
}

void MainWindow::onDeviceStatusChanged(const bool tf,QString msg)
{
    if(!msg.isEmpty())
    {
        ui->serialPortStatus->setText(msg);
    }
    if(!tf)
    {
        m_deviceStatusTimer->start();
    }else {
        m_deviceStatusTimer->stop();
    }
}

void MainWindow::deviceStatusTimeout()
{
    if(ui->serialPortStatus->styleSheet().compare(whiteSS))
    {
        ui->serialPortStatus->setStyleSheet(whiteSS);
    }else {
        ui->serialPortStatus->setStyleSheet(redSS);
    }
}

bool MainWindow::loadJson()
{
    QFile loadFile(QStringLiteral("deviceInfo.json"));

    if(!loadFile.open(QIODevice::ReadOnly)){
       qWarning("Could not open json file to read");
       return false;
    }
    QByteArray loadData = loadFile.readAll();
    loadFile.close();
    QJsonDocument loadDoc(QJsonDocument::fromJson(loadData));

    QJsonObject jsonObj = loadDoc.object();

    int portIndex = jsonObj["portIndex"].toInt();
    QString portString = jsonObj["portString"].toString();

    if(ui->cbSerialPorts->count() <= portIndex)
        return false;
    if(ui->cbSerialPorts->itemText(portIndex).compare(portString) == 0)
    {
        ui->cbSerialPorts->setCurrentIndex(jsonObj["portIndex"].toInt());
        ui->cbBaudrate->setCurrentIndex(jsonObj["baudrate"].toInt());
        ui->cbPollingInterval->setCurrentIndex(jsonObj["polling"].toInt());

    }
    return true;
}
void MainWindow::saveJson()
{
    QJsonObject jsonObj;
    jsonObj["portIndex"] = ui->cbSerialPorts->currentIndex();
    jsonObj["portString"] = ui->cbSerialPorts->currentText();
    jsonObj["baudrate"] = ui->cbBaudrate->currentIndex();
    jsonObj["polling"] = ui->cbPollingInterval->currentIndex();

    QJsonDocument saveDoc(jsonObj);
    QFile saveFile(QStringLiteral("deviceInfo.json"));
    if(!saveFile.open(QIODevice::WriteOnly)){
        qWarning("Cound not open json file to save");
        return;
    }
    saveFile.write(saveDoc.toJson());
    saveFile.close();
}
