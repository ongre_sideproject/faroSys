#include "DataTableModel.h"
#include <QDebug>
#include <QSqlError>

DataTableModel::DataTableModel(QSqlDatabase db)
{
    m_tableModel=new QSqlQueryModel;
    m_db = db;
    //m_db.open();
}
QVariant DataTableModel::data(const QModelIndex &idx, int role) const
{
    if(role == Qt::TextAlignmentRole)
        return Qt::AlignCenter;
    else if(role == Qt::DisplayRole && (idx.column() == 3 || idx.column() == 5))
    {
        return  QString::number(QIdentityProxyModel::data(idx, role).toDouble(),'f',2);
    }
    return QIdentityProxyModel::data(idx, role);
}
bool DataTableModel::setTable(QString tableName)
{

    QString raw = QString("raw_%1").arg(tableName);
    QString setup = QString("setup_%1").arg(tableName);
    QString query = QString("select BYDY_NO,'%1'.TARGET_NO,SYS_TIME,W_AMP,W_TIME,AMP,TIME,WATER_TEMP from \
                            '%1' INNER JOIN '%2' on '%1'.TARGET_NO = '%2'.TARGET_NO").arg(raw).arg(setup);

    m_tableModel->setQuery(query,m_db);

    if(m_tableModel->lastError().isValid())
    {
        qDebug() << m_tableModel->lastError();
        return false;
    }
    this->setSourceModel(m_tableModel);
    return true;
}
/*
select BYDY_NO,t1.TARGET_NO,SYS_TIME,W_AMP,W_TIME,AMP,TIME,WATER_TEMP,
    case
        when (ABS(AMP - W_AMP) > 1) then
            '불량'
        when (ABS(AMP - W_AMP) > 0.6) then
            '경고H'
        when (ABS(AMP - W_AMP) > 0.2) then
            '경고L'
        ELSE
            '정상'
        end
    from
        'raw_2020-000002' as t1
        INNER JOIN 'setup_2020-000002' on t1.TARGET_NO = 'setup_2020-000002'.TARGET_NO
 */
