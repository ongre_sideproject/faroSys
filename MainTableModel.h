#ifndef MAINTABLEMODEL_H
#define MAINTABLEMODEL_H

#include <QObject>
#include <QSqlTableModel>
#include <QIdentityProxyModel>

class MainTableModel : public QIdentityProxyModel
{
    Q_OBJECT
public:
    MainTableModel(QSqlDatabase db = QSqlDatabase());
    QVariant data(const QModelIndex &idx, int role = Qt::DisplayRole) const override;
    bool setTable(QString tableName);
    bool onSave();
    bool append(QString device_ID);
    int isContins(QString device_ID) const;
    QString getDeviceID(int rowIndex) const;
    QStringList getStringData(int rowIndex) const;
    bool setStringData(int rowIndex, QStringList list);

private:
    QSqlTableModel *m_tableModel;
};

#endif // MAINTABLEMODEL_H
