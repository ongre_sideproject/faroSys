#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSerialPortInfo>
#include "SetupTableModel.h"
#include "DataTableModel.h"
#include "EventTableModel.h"
#include "MainTableModel.h"
#include "Defines.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE


typedef enum VIEW_ENUMS
{
    MAIN_VIEW,
    SETUP_VIEW,
    EVENT_VIEW,
    DATA_VIEW
}VIEW_ENUM;


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void onSerialPortListChanged(const QList<QSerialPortInfo> list);
    void onAppendData(const QString rawData);
    void onSerialPortStatusChanged(const bool status);
    void onSendData(const QString data);
    void onDeviceStatusChanged(const bool tf,QString msg);

signals:
    void connectSerialPortByName(const QString portName, const int baudrate, const int interval, QSqlDatabase db);
    void saveFunction(const DataTableModel* dataTableModel, const QString filename);
    void startFunction(bool tf);
    void addData(const DataStruct data, bool isNew);
    void safeQuit();

private slots:
    void on_btnConnect_clicked();
    void on_btnClose_clicked();
    void on_btnSearch_clicked();
    void on_btnCancel_clicked();
    void onClickedMainTableCell(const QModelIndex &);
    void on_btnBackButton_clicked();
    void on_btnExportExcel_clicked();

    void on_btnTest_2_clicked();
    void on_btnMainTest_clicked();
    void on_btnMainTest_1_clicked();

    void deviceStatusTimeout();

private:
    Ui::MainWindow  *   ui;
    SetupTableModel *   m_SetupModel;
    DataTableModel  *   m_DataModel;
    EventTableModel *   m_EventModel;
    MainTableModel  *   m_MainModel;
    QSqlDatabase        m_db;
    int                 m_currentRow;
    QTimer          *   m_deviceStatusTimer;
    QTimer          *   check;


    void initDB();
    void initMainTable();
    void initSetupTable(QString tableName);
    void initDataTable(QString tableName);
    void initEventTable(QString tableName);
    void initSubUI(VIEW_ENUM status);
    void onResizeColumn();
    void setTitle(int index);
    bool loadJson();
    void saveJson();

};
#endif // MAINWINDOW_H
