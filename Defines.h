#ifndef DEFINES_H
#define DEFINES_H
#include <QString>

typedef struct DataStructs
{
   QString deviceID;
   QString sysTime;
   QString targetNo;
   QString amp;
   QString time;
   QString waterTemp;
}DataStruct;


#endif // DEFINES_H
