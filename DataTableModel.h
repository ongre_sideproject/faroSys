#ifndef DATATABLEMODEL_H
#define DATATABLEMODEL_H

#include <QObject>
#include <QSqlQueryModel>
#include <QIdentityProxyModel>

class DataTableModel : public QIdentityProxyModel
{
    Q_OBJECT
public:
    DataTableModel(QSqlDatabase db = QSqlDatabase());

    QVariant data(const QModelIndex &idx, int role = Qt::DisplayRole) const override;
    bool setTable(QString tableName);

private:
    QSqlQueryModel *    m_tableModel;
    QSqlDatabase        m_db;
};

#endif // DATATABLEMODEL_H
