#ifndef SETUPSQLTABLEMODEL_H
#define SETUPSQLTABLEMODEL_H

#include <QObject>
#include <QSqlTableModel>
#include <QIdentityProxyModel>

class SetupTableModel : public QIdentityProxyModel
{
    Q_OBJECT
public:
    SetupTableModel(QSqlDatabase db = QSqlDatabase());
    QVariant data(const QModelIndex &idx, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;
    bool setTable(QString tableName);
    bool onSave();
private:
    QSqlTableModel *m_tableModel;

};

#endif // SETUPSQLTABLEMODEL_H
