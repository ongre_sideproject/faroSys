#include "BackEnd.h"
#include <QFile>
#include <QDebug>
#include <QDateTime>
#include <QSqlQuery>
#include <QSqlError>
#include <QThread>
BackEnd::BackEnd(QObject *parent) : QObject(parent)
{

}
void BackEnd::initThread()
{
    m_serialInfoTimer = new QTimer;
    m_serialInfoTimer->setInterval(1500);
    connect(m_serialInfoTimer,SIGNAL(timeout()),this,SLOT(onSerialInfoTimeout()));
    m_serialInfoTimer->start();

    m_serialPortStatusTimer = new QTimer;
    m_serialPortStatusTimer->setInterval(1500);
    m_serialPortStatusTimer->start();

    m_functionTimer = new QTimer;
    connect(m_functionTimer,SIGNAL(timeout()),this,SLOT(onFuntionTimeout()));


    m_serialPort = new QSerialPort;

    m_currentIndex = 1;
    m_prevIndex = 0;
    m_dbActive = false;
    m_isQuit = false;
    m_isDeviceStatus = true;

    onSerialInfoTimeout();

}
BackEnd::~BackEnd()
{
}
void BackEnd::onSerialInfoTimeout(void)
{
    QList<QSerialPortInfo> list = QSerialPortInfo::availablePorts();
    if(m_serialPortList.count() != list.count())
    {
        m_serialPortList = list;
        emit serialPortListChanged(m_serialPortList);
    }
}

bool BackEnd::isSerialOpen(void)
{
    if(m_serialPort == NULL)
    {
        return false;
    }
    return m_serialPort->isOpen();
}

void BackEnd::onSerialPortStatusTimeout(void)
{
    bool tmp = isSerialOpen();
    if(tmp != m_status)
    {
        m_status = tmp;
        emit serialStatusChanged(m_status);
    }
    if(!tmp)
    {
        disconnect(m_serialPort,SIGNAL(readyRead()),this,SLOT(onCanReadSerialPort()));
    }
    deviceStatusChanged(true);
}
void BackEnd::onConnectSerialPortByName(const QString portName, const int baudrate, const int interval, QSqlDatabase db)
{
    if(isSerialOpen())
    {
        m_functionTimer->stop();
        m_serialPort->close();
        onSerialPortStatusTimeout();
        return;
    }

    Q_FOREACH (const QSerialPortInfo& port, m_serialPortList)
    {
        if(port.portName() == portName)
        {
            m_serialPort->setPort(port);
            m_serialPort->setBaudRate(baudrate);
            break;
        }
    }
    bool tf = m_serialPort->open(QIODevice::ReadWrite);
    m_db = db;

    if(tf)
    {
        m_currentIndex = 1;
        if(setRTC())
        {
            m_functionTimer->start(interval);
        }else {
            deviceStatusChanged(false,"통신에러");
            m_serialPort->close();
            return;
        }


    }else {
        m_functionTimer->stop();
    }

    onSerialPortStatusTimeout();
}

void BackEnd::onCanReadSerialPort(void)
{
    m_buffer.append(QString(m_serialPort->readAll()));

    while(m_buffer.count("\r\n"))
    {
        int endIndex = m_buffer.indexOf("\r\n");
        QString tmp = m_buffer.left(endIndex);

        if(tmp.contains("~Rk "))
        {
            tmp.remove("~Rk ");
            emit appendData(tmp);
            m_serialPort->write(">OK\r\n");
        }else if(tmp.contains("~Ek "))
        {
            m_serialPort->write(">OK\r\n");
        }
        m_buffer.remove(0, endIndex+2);
    }
}
void BackEnd::onSaveFunction(const DataTableModel* dataTableModel, const QString filename)
{
    m_mutex.lock();
    QString extention = filename.right(5);
    if(extention.contains("csv"))
    {
        exportByCSV(dataTableModel, filename);
    }else if(extention.contains("xlsx"))
    {
        //exportByCOM(tablewidget,filename);
    }
    m_mutex.unlock();
}

void BackEnd::exportByCSV(const DataTableModel* dataTableModel, QString fileName)
{
    QFile f(fileName);

    if (f.open(QFile::WriteOnly | QFile::Truncate))
    {
        QTextStream data( &f );
        QStringList strList;
        //"","11","2020-11-20 23:01:12","7.50","11","9.50","12","25"

        QString header = "'BYDY NO','타점 번호','용접 일시','용접 전류','용접 시간','용접 전류','용접 시간','냉각수 온도'\n";

        data << header.replace("'","\"");

        for( int r = 0; r < dataTableModel->rowCount(); ++r )
        {
            strList.clear();
            for( int c = 0; c < dataTableModel->columnCount(); ++c )
            {
                strList << "\"" + dataTableModel->data(dataTableModel->index(r,c)).toString()+"\"";
            }
            data << strList.join( "," )+"\n";
        }
        f.close();
    }else
    {
        qDebug() << f.errorString();
    }

}
void BackEnd::onStartFunction(bool tf)
{
    if(!isSerialOpen())
    {
        m_functionTimer->stop();
        return;
    }
    if(tf)
    {
        m_functionTimer->start();
    }else
    {
        m_functionTimer->stop();
    }
}
bool BackEnd::setRTC()
{
    if(!isSerialOpen())
    {
        return false;
    }
    QByteArray tmp = ">Ak";
    tmp.append("\r\n");
    m_serialPort->write(tmp);
    m_serialPort->waitForBytesWritten();
    this->thread()->msleep(100);
    m_serialPort->waitForReadyRead(500);
    QString read = (QString(m_serialPort->readAll()));
    if(!read.contains("~Ok\r\n"))
    {
        return false;
    }

    tmp = ">RTC ";
    tmp.append(QDateTime::currentDateTime().addDays(1).toString("yyyy-MM-dd hh:mm:ss").toLatin1());
    tmp.append("\r\n");
    m_serialPort->write(tmp);
    m_serialPort->waitForBytesWritten();
    this->thread()->msleep(100);
    m_serialPort->waitForReadyRead(500);
    read = (QString(m_serialPort->readAll()));
    if(!read.contains("~Ok\r\n"))
    {
        return false;
    }
    connect(m_serialPort,SIGNAL(readyRead()),this,SLOT(onCanReadSerialPort()));
    return true;
}
void BackEnd::onFuntionTimeout(void)
{
    if(!isSerialOpen())
    {
        return;
    }

    /*
    if(!m_prevTime.isValid())
    {
        m_prevTime = QDateTime::currentDateTime();
    }else {
        qDebug() << m_prevTime.msecsTo(QDateTime::currentDateTime());
        m_prevTime = QDateTime::currentDateTime();
    }
    */

    if(m_prevIndex == m_currentIndex && m_isDeviceStatus)
    {
        qDebug() << "Data Timeout";
        m_isDeviceStatus = false;
        emit deviceStatusChanged(m_isDeviceStatus);
    }else if(m_prevIndex != m_currentIndex && !m_isDeviceStatus ){
        m_isDeviceStatus = true;
        emit deviceStatusChanged(m_isDeviceStatus);
    }

    QString tmp = ">Dk ";
    tmp.append(QString::number(m_currentIndex).rightJustified(6,'0'));
    tmp.append("\r\n");
    m_serialPort->write(tmp.toLatin1());
    m_prevIndex = m_currentIndex;

    emit sendData(tmp.remove("\r\n"));
}
void BackEnd::onAddData(const DataStruct data, bool isNew)
{
    if(m_isQuit)
        return;
    m_mutex.lock();
    m_dbActive = true;
    if(isNew)
    {
        createNewTable(data.deviceID,m_db);
    }
    appendRawData(data,m_db);

    int index = data.deviceID.split("-").at(1).toInt();


    m_currentIndex = index+1;
#if 1
    if(m_currentIndex > 256)
    {
        m_currentIndex = 1;
    }
#else
    if(m_currentIndex > 5)
    {
        m_currentIndex = 1;
    }
    qDebug() << __FUNCTION__ << tmp << index << m_currentIndex;

#endif
#if 1
    if(m_currentIndex % 20 == 0)
    {
        if(!m_db.commit())
        {
            qDebug() << __FUNCTION__ << m_db.lastError().text();
            qDebug() << __FUNCTION__ << m_db.rollback();
        }
        m_db.transaction();
    }
#endif

    m_dbActive = false;
    m_mutex.unlock();
}
void BackEnd::createNewTable(QString tableName, QSqlDatabase db)
{
    QSqlQuery query(db);

    //설정 테이블 생성
    QString tmp = QString("CREATE TABLE 'setup_%1' AS SELECT * FROM tmp").arg(tableName);
    query.prepare(tmp);
    if(!query.exec()){
        qWarning() << __FUNCTION__  << query.lastQuery() << query.lastError().text();
        return;
    }
    //로우 데이터 테이블 생성
    tmp = QString("CREATE TABLE 'raw_%1' ( \
                   'SYS_TIME' TEXT NOT NULL, \
                   'TARGET_NO' INTEGER NOT NULL, \
                   'AMP' NUMERIC NOT NULL, \
                   'TIME' INTEGER NOT NULL, \
                   'WATER_TEMP' INTEGER NOT NULL \
                    );").arg(tableName);
    query.prepare(tmp);
    if(!query.exec()){
        qWarning() << __FUNCTION__   << query.lastQuery() << query.lastError().text();
        return;
    }
}
void BackEnd::appendRawData(const DataStruct data, QSqlDatabase db)
{
    QSqlQuery query(db);
    QString tmp = QString("INSERT INTO 'raw_%1' VALUES('%2',%3,%4,%5,%6)") \
            .arg(data.deviceID).arg(data.sysTime).arg(data.targetNo).arg(data.amp).arg(data.time).arg(data.waterTemp);
    query.prepare(tmp);
    if(!query.exec()){
        qWarning() << __PRETTY_FUNCTION__ << "qurey error";
        return;
    }
}

void BackEnd::safeQuit()
{
    m_mutex.lock();
    while (m_dbActive);
    m_isQuit = true;
    m_mutex.unlock();
}
/*

INSERT INTO 'raw_2020-000001' (SYS_TIME,TARGET_NO,AMP,TIME,WATER_TEMP,GRADE,COLOR)
SELECT '%2',%3,%4,%5,%6,//grade,//color
    case
        when (ABS(t1.W_AMP - %4) > 1) then
            '불량'
        when (ABS(t1.W_AMP - %4) > 0.6) then
            '경고H'
        when (ABS(t1.W_AMP - %4) > 0.2) then
            '경고L'
        ELSE
            '정상'
        end
        from 'setup_2020-000001' t1 WHERE t1.TARGET_NO = 1
 */
